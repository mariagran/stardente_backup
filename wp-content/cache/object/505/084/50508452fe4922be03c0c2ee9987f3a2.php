�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:43;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-21 14:45:37";s:13:"post_date_gmt";s:19:"2017-11-21 16:45:37";s:12:"post_content";s:4367:"<h2>O que é Limpeza Dental?</h2>
A Limpeza Dental – também conhecida como profilaxia dental – é o tratamento responsável pela prevenção das doenças bucais e periodontais. É um procedimento relativamente rápido e indolor, que deve ser feito periodicamente com o seu dentista.

A Limpeza Dental consiste na remoção da placa bacteriana, tártaro e pequenas manchas, e, apesar de não ser um tratamento especifico voltado ao <a href="https://www.stardente.com.br/tratamentos/clareamento-dental/">clareamento dental</a>, também pode ajudar a reduzir o amarelado dos dentes.
<h2>Quais as vantagens da Limpeza Dental?</h2>
A Limpeza Dental é um procedimento que pode trazer diversos benefícios para o paciente, dentre esses, podemos citar:
<ul>
 	<li>Prevenção de doenças da gengiva;</li>
 	<li>Ajuda a proteger os dentes contra cáries, tártaro e placa bacteriana;</li>
 	<li>Reduz as manchas nos dentes causadas por alimentos;</li>
 	<li>Combate o mau hálito;</li>
 	<li>Auxilia na prevenção de outros problemas dentais.</li>
</ul>
<h2>Por que Fazer a Limpeza Dental em Consultório?</h2>
Por mais que o paciente mantenha uma rotina de higiene bucal adequada (com escovações à cada refeição e o uso fio dental e enxaguantes bucais), ainda é possível que ocorra o acúmulo de placa bacteriana ou que surjam manchas nos dentes causadas por alguns alimentos.

Por isso, é tão importante fazer, periodicamente, uma Limpeza Dental no consultório do Dentista. O procedimento é realizado com materiais e produtos específicos para o uso profissional, com os dentes sendo higienizados individualmente, o que garante um resultado imediato e um sorriso mais saudável.

Além disso, durante o tratamento de profilaxia, o dentista também fará a avaliação da saúde geral da boca do paciente, podendo diagnosticar outros problemas que deverão ser tratados, como cáries, gengivites ou até mesmo a necessidade de extrações ou de um <a href="https://www.stardente.com.br/tratamentos/tratamento-de-canal/">tratamento de canal</a>.
<h2>Como é feito a Profilaxia Dental?</h2>
A Limpeza Dental é composta por uma série de procedimentos, os mais comuns são:
<ul>
 	<li><strong>Raspagem do Tártaro (ou tartarectomia)</strong> – É comumente o primeiro passo da Limpeza Dental realizada pelo Dentista, e basicamente consiste na remoção do tártaro dos dentes – que são tratados individualmente. É feita com o uso de instrumentos específicos como ultrassom, curetas, tiras de aço e lâminas.</li>
 	<li><strong>Jateamento de bicarbonato de sódio</strong> – Após a raspagem, o dentista faz uso de um jato de bicarbonato de sódio nos dentes, que vai auxiliar na remoção mais profunda da placa bacteriana, além de evitar e/ou prevenir as inflamações na gengiva.</li>
 	<li><strong>Polimento</strong> – Juntamente com a etapa do jateamento, é possível que seja feita um polimento dos dentes, para reduzir as possíveis manchas existentes na superfície dos dentes.</li>
 	<li><strong>Escovação Profissional Completa</strong> – Utilizando uma escova rotatória especial e um creme dental específico para uso profissional, o dentista irá completar a o procedimento de Limpeza Dental.</li>
 	<li><strong>Aplicação de Flúor</strong> – Para finalizar, o profissional irá fazer a aplicação do Flúor, que vai auxiliar no fortalecimento e na preservação dos dentes.</li>
</ul>
<h2>Quando Fazer a Limpeza Dentária?</h2>
Na maior parte dos casos, é recomendado que a Limpeza Dental seja realizada a cada 6 meses, porém esse tempo pode ser menor, dependendo de outros fatores, como o uso de <a href="https://www.stardente.com.br/tratamentos/aparelho-ortodontico/">aparelho dental</a> ou em casos de gengivite. Por essa razão, recomendamos que consulte o seu dentista para saber qual a frequência ideal para o seu caso.

<hr />

<h3><span style="color: #999999;">Limpeza Dental em Curitiba e Araucária?</span></h3>
<span style="color: #999999;">A <strong>Stardente</strong> possui uma equipe de dentistas extremamente qualificada, pronta para lhe atender!</span>
<span style="color: #999999;">Consultórios Odontológicos em Curitiba (Centro e Novo Mundo) e Araucária (Centro).
Escolha a sede mais próxima e agende agora mesmo sua consulta, usando o formulário abaixo:</span>";s:10:"post_title";s:14:"Limpeza Dental";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"limpeza-dental";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-05-06 13:11:39";s:17:"post_modified_gmt";s:19:"2019-05-06 16:11:39";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:67:"http://localhost/projetos/stardente/?post_type=tratamento&#038;p=43";s:10:"menu_order";i:6;s:9:"post_type";s:10:"tratamento";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}