�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:48;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-05-01 14:00:06";s:13:"post_date_gmt";s:19:"2019-05-01 17:00:06";s:12:"post_content";s:7511:"<h2>O que é Ortodontia?</h2>
A ortodontia é a área da odontologia que previne, trata e corrige os problemas de posicionamento dos dentes e dos ossos maxilares, através do uso de aparelhos ortodônticos, entre outros.

O principal objetivo de um tratamento ortodôntico é restabelecer o perfeito encaixe dos dentes superiores e inferiores, indispensável para uma correta mastigação. Além disso, a ortodontia possui uma importante função estética, pois os dentes desalinhados comumente prejudicam a harmonia da face e comprometem a aparência do sorriso.
<h2>Aparelho Dental, quando o uso é indicado?</h2>
O uso do Aparelho Ortodôntico (ou aparelho dental, como é popularmente conhecido) é indicado para corrigir o posicionamento dos dentes, como em casos de <a href="https://www.stardente.com.br/tratamentos/oclusao-dentaria/">má oclusão</a> ou <a href="https://www.stardente.com.br/tratamentos/fechamento-de-diastemas/">diastemas</a>, por exemplo.

Apenas o profissional ortodontista poderá afirmar se o paciente deve ou não utilizar o aparelho ortodôntico e qual o modelo mais indicado para o caso.
<h2>Quais os problemas que podem ser tratados com o uso do Aparelho Ortodôntico?</h2>
Dentre os problemas mais comuns tratados com a utilização do aparelho dental estão:
<ul>
 	<li>Sobremordida (ou Mordida Profunda) – É possivelmente o problema mais comum de quem busca um tratamento ortodôntico. Trata-se dos dentes salientes, onde os dentes superiores recobrem quase totalmente os inferiores, causando problemas mastigatórios, além de problemas estéticos.</li>
 	<li>Mordida Cruzada – Ocorre quando não há o encaixe perfeito entre as arcadas dentarias superiores e inferiores, causando dificuldade da mastigação e desvios estéticos, além de aumentar a possibilidade desenvolvimento de bruxismo.</li>
 	<li>Mordida Aberta – Nesse caso, os dentes superiores não tocam os dentes inferiores durante a mastigação.</li>
 	<li>Desvio de Linha Mediana – O conhecido sorriso torto, que ocorre quando o centro da arcada dentária superior não está devidamente alinhado com o centro da arcada inferior.</li>
 	<li>Diastema – São as falhas ou espaços entre os dentes, que podem ser provenientes da ausência de algum dente ou do preenchimento inadequado da arcada dentária.</li>
 	<li>Apinhamento – Ao contrário do diastema, nesse caso existe a falta de espaço para o correto acomodamento dos dentes na arcada dentária.</li>
</ul>
<h2>Como é o Tratamento Ortodôntico?</h2>
Após a avaliação do histórico dentário do paciente e da realização de fotografias e radiografias, o profissional irá confeccionar os moldes de gesso da arcada dentária, que auxiliarão no mapeamento do formato e posicionamento dos dentes e da arcada dentária.

Somente então, o ortodontista irá definir um plano de tratamento adequado para o caso, com a colocação do aparelho e demais procedimentos que se mostrem necessários.
<h2>Tipos de Aparelho Ortodôntico</h2>
Atualmente existem diversos tipos de aparelhos que podem ser utilizados durante o tratamento ortodôntico. Os mais comuns são:
<ul>
 	<li><strong>Aparelho Ortodôntico Fixo</strong> – É o mais comum, onde os bráquetes - colados individualmente nos dentes - são ligados por um fio metálico. É bastante eficaz para a correção do alinhamento dos dentes, pois vai movimentando-os pouco a pouco até a posição adequada. Esse tipo de aparelho necessita de manutenção mensal no consultório do dentista e exige maior cuidado com a higienização.</li>
 	<li><strong>Aparelho Ortodôntico Estético</strong> – A função é a mesma do aparelho fixo, com a vantagem de ser muito mais discreto que a versão metálica. Nesse modelo de aparelho, os bráquetes são feitos de materiais transparentes (como resina ou safira), ou que imitam a coloração dos dentes (porcelana). Normalmente, o preço desse tipo de aparelho ortodôntico é um pouco maior que o convencional, mas para alguns pacientes, a discrição e o efeito estético compensam o investimento.</li>
 	<li><strong>Aparelho Ortodôntico Lingual</strong> – Uma outra variação do aparelho fixo tradicional. Nele, os bráquetes são fixados na parte interna dos dentes. O tratamento com esse tipo de aparelho é geralmente mais demorado quando comparado ao aparelho tradicional, é seu uso é comumente indicado para pacientes que praticam esportes com maior contato físico, pois reduz a chance de cortes em eventuais acidentes.</li>
 	<li><strong>Aparelho Móvel</strong> – Esse modelo é indicado para manter os dentes na posição correta, e por esse motivo é bastante recomendado após a retirada do aparelho fixo, complementando o tratamento. Também é indicado para crianças de até 12 anos, que estão com a arcada dentária em desenvolvimento.</li>
</ul>
<h2>Qual a idade para usar Aparelho Ortodôntico?</h2>
Não existe limitação de idade para o uso de Aparelho Ortodôntico.  No caso das crianças, a partir dos 6 anos de idade pode haver a indicação do uso de aparelhos removíveis, para correção e prevenção de desvios da arcada dentária.

Para os adultos, não há idade mínima ou máxima para iniciar o uso do aparelho ortodôntico – a única variável nesses casos é a de que – em boa parte dos casos - quanto mais tardio for o início, maior o tempo de duração do tratamento.
<h2>O tratamento com o Aparelho Ortodôntico dói?</h2>
Podemos afirmar que, num panorama geral, o tratamento ortodôntico não dói.

O que pode ocorrer é um desconforto durante os dias após a colocação do aparelho ou depois das manutenções mensais, devido à pressão exercida na arcada dentária. Nesses casos, o recomendável é evitar o consumo de alimentos sólidos e fazer o uso de analgésicos, caso o paciente julgue necessário.
<h2>Por quanto tempo é preciso usar o Aparelho Dental?</h2>
A duração do tratamento com o uso do Aparelho Ortodôntico depende muito do objetivo e do problema a ser tratado, podendo variar de alguns meses até anos. Apenas o profissional ortodontista poderá determinar por quanto tempo o paciente precisará usar o aparelho dental.
<h2>Quais os cuidados necessários durante o uso do Aparelho Ortodôntico?</h2>
Durante o tratamento ortodôntico o paciente deve ter especial atenção à correta higienização do aparelho dental. Além da escovação diária, e do uso do fio dental, é recomendado o uso de escovas interdentais, que facilitam a limpeza dos espaços entre os dentes e os bráquetes do aparelho.

Aconselha-se também fazer periodicamente uma <a href="https://www.stardente.com.br/tratamentos/limpeza-dental/">limpeza dental</a> profissional, para evitar o surgimento de problemas bucais, como a gengivite ou o acúmulo de placa bacteriana.

Com relação à alimentação, é preciso estar atento: deve-se evitar alimentos muito duros - que podem quebrar ou descolar o aparelho – e os doces, como balas e chicletes – que irão grudar no aparelho e dificultar a higienização.

<hr />

<h3><span style="color: #999999;">Aparelho Ortodôntico em Curitiba e Araucária? </span></h3>
<span style="color: #999999;">A Stardente possui uma equipe de ortodontistas extremamente qualificada, pronta para lhe atender! Visite um de nossos consultórios odontológicos em Curitiba (Centro e Novo Mundo) ou em Araucária e faça sua avaliação. Se preferir, agende agora mesmo através do formulário:</span>";s:10:"post_title";s:21:"Aparelho Ortodôntico";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"aparelho-ortodontico";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-05-06 13:19:53";s:17:"post_modified_gmt";s:19:"2019-05-06 16:19:53";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:67:"http://localhost/projetos/stardente/?post_type=tratamento&#038;p=48";s:10:"menu_order";i:1;s:9:"post_type";s:10:"tratamento";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}