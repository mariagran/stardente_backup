�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"146";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-23 11:36:28";s:13:"post_date_gmt";s:19:"2017-11-23 13:36:28";s:12:"post_content";s:2501:"A saúde do seu sorriso é essencial para uma vida mais alegre e uma autoestima elevada e, para isso é necessária uma higienização diária.

Muitas pessoas pela correria do dia a dia esquecem-se de dar a atenção necessária para a escovação dos dentes, aumentando a probabilidade de ter gengivite.
<h3><strong>Mas, o que é Gengivite?</strong></h3>
A gengivite é uma inflamação que atinge a gengiva pelo acúmulo de resíduos entre os dentes, que formam placas bacterianas.

Quando não há uma higienização correta diariamente essas placas aumentam e ficam cada vez mais difíceis de serem retiradas com o fio dental ou escovação, formando tártaros que só podem ser retirados por dentistas.

Essas placas, quando não retiradas produzem ácidos que em contato com a gengiva dão origem a gengivite, que se não for cuidada ainda em casa podem progredir para a periodontite, doença mais agravada que deve ser tratada sobre observação odontológica.
<h3><strong>Quais os sintomas?</strong></h3>
Existem sintomas clássicos e fáceis de reconhecer no início da gengivite. Se você perceber um inchaço na gengiva, uma vermelhidão acima do normal e principalmente, se há sangramento quando passa o fio dental, provavelmente você tenha este problema.
<h3><strong>Como tratar a Gengivite?</strong></h3>
O tratamento da gengivite é simples, de acordo com o grau da doença.

Primeiramente, recomendamos que procure um dentista para avaliar e diagnosticar se de fato é gengivite, ele vai prescrever remédios para bochecho, se necessário, e você poderá fazer o procedimento em casa.

Faça uma escovação minuciosa em todos os dentes - lembre-se, <a href="https://www.stardente.com.br/escova-de-dentes-dicas-para-escovacao-ideal/">escova sempre de cerdas macias</a> com movimentos circulares - não force a gengiva para não machuca-la mais, afinal, ela está muito sensível.

Após a escovação, ou antes se preferir, passe o fio dental em todos os dentes com calma, certificando-se de que não há nenhum resíduo entre os dentes.

A higienização dos dentes deve ser feita 30 min após as refeições, para que o PH da sua saliva entre em contato com o ácido dos alimentos e seu esmalte dos dentes não seja comprometido.

Assim, a gengivite pode ser facilmente revertida, uma higiene diária garante a saúde bucal que você precisa. E aqui na <strong>Stardente</strong> temos dentistas especializados que podem te auxiliar, faça uma avaliação e cuide do seu sorriso.";s:10:"post_title";s:51:"A Gengivite Pode Colocar sua Saúde Bucal em Perigo";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:48:"gengivite-pode-colocar-sua-saude-bucal-em-perigo";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-07-10 17:33:16";s:17:"post_modified_gmt";s:19:"2019-07-10 20:33:16";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:77:"http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/stardente/?p=146";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}