�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"152";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-23 11:38:19";s:13:"post_date_gmt";s:19:"2017-11-23 13:38:19";s:12:"post_content";s:2909:"<!-- wp:paragraph -->
<p>O siso (terceiro molar) é o último dente a nascer, é responsável pelo alto índice de extrações feitas ultimamente. Tem “o nascimento” tardio, geralmente entre os 16 e 20 anos e, com três formas de erupção:</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Normal:</em></strong> Nasce alinhado à arcada, auxiliando a mastigação.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Inclusos:</em></strong> Quando ele não consegue sair, ficando preso entre o osso e a gengiva. Por estar completamente “deitado” ou quando não há espaço entre os demais dentes, pode causar inflamações, cistos e dores de cabeça, face e ouvido.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><em>Semi- inclusos:</em></strong> Nasce parcialmente, provocando o apinhamento dos demais dentes, facilitando o acúmulo de resíduos, podendo provocar cáries e a pericoronarite.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Há também, sisos que não saem pela ausência do nascimento do “germe dentário” (responsável pela formação do dente). Embora poucas pessoas tenham essa vantagem, algumas não sentem incômodos ou dor.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Mas, é necessário extrair o siso?</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>É necessário um diagnóstico do seu dentista, através de radiografias é possível visualizar a formação do dente e, decidir pela extração ou não.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Extração do Siso - Recuperação pós-cirúrgica:</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Se você vai <a href="https://www.stardente.com.br/tratamentos/extracao-de-siso/">extrair o siso</a> é importante se manter atento a alguns cuidados para uma boa recuperação: primeiramente fique ciente que após a cirurgia hematomas e inchaços são comuns, bolsas de gelo e remédios (com prescrição do seu cirurgião) podem aliviar o desconforto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Prefira comidas sempre frias ou em temperatura ambiente, abuse dos sorvetes, pudins, e outros alimentos que não forcem a mastigação. Evite ingerir alimentos ou bebidas quentes, que podem remover o coágulo e facilitar infecções, prejudicando bastante sua recuperação.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lembre-se também, consultas de rotina após a operação são importantes, tire todas as dúvidas com seu cirurgião dentista e não se esqueça de sempre enxaguar a área de extração após as refeições para evitar infecções.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aqui na <strong>Stardente</strong> você tem acompanhamento completo, desde as primeiras consultas até o pós-operatório. Converse com um de nossos profissionais e cuide do seu sorriso com segurança.</p>
<!-- /wp:paragraph -->";s:10:"post_title";s:33:"Siso: cuidados após a extração";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:27:"siso-cuidados-apos-extracao";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-07-10 18:02:59";s:17:"post_modified_gmt";s:19:"2019-07-10 21:02:59";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:77:"http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/stardente/?p=152";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}