�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"140";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-23 11:34:54";s:13:"post_date_gmt";s:19:"2017-11-23 13:34:54";s:12:"post_content";s:2512:"A higiene bucal das crianças é um passo muito importante para o desenvolvimento deles, uma fase que precisa de atenção e supervisão constante dos pais.

Mas, eles têm cuidado bem da escovação infantil? Aqui falaremos algumas dicas que podem auxiliar no processo de escovação infantil, até que aprendam a continuar sem a supervisão.

Antes de tudo, é importante que você seja o exemplo! Então cuide bem da sua saúde bucal e seu filho(a) perceberá a importância de cuidar bem da dele(a). No início da infância é muito comum que as crianças reproduzam os mesmos atos que os pais, por isso, tudo que ele vê em você vai querer fazer também.
<h3>Como transformar a higiene bucal em algo divertido?</h3>
Reconhecemos que para os pequenos, ações que envolvam a higiene, em sua grande maioria, não são bem vindas - como o banho, lavar as mãos ou escovar os dentes.

Uma boa dica é usar objetos que agucem a curiosidade - cores e personagens divertidos são sempre bem vindos.

Invista em escovas com cerdas macias e coloridas, leve a criança junto e deixe que escolha a que mais chama a atenção. Use cremes dentais com gostos destinados ao público infantil, sempre com a quantidade permitida de flúor para que auxilie na prevenção de cáries e placas.
<h3>Passos para uma boa escovação infantil:</h3>
<ul>
 	<li>Lembre-se de fazer a escovação nas crianças no mínimo 3 vezes ao dia, isso cria uma rotina e auxilia que memorize a importância da repetição.</li>
 	<li>Fio dental é muito importante, principalmente para uma arcada dentária em formação. Por isso, dos 4 a 8 anos fica como responsabilidade dos pais, a partir dos 8 a criança já pode cuidar da higiene sozinha.</li>
 	<li>Produtos que contenham flúor, sejam no creme dental ou na água, são importantíssimos neste momento. Eles garantem que as placas não fixem nos dentes e reduzem o surgimento de cáries.</li>
 	<li>Alimentação balanceada - o que seu filho ingere reflete no seu sorriso. Por isso, garanta que sua alimentação seja repleta de vitaminas, ferro e cálcio. Tente mantê-lo longe de alimentos com muito açúcar ou ácidos.</li>
 	<li>Visitas regulares ao dentista são importantes para o desenvolvimento sadio e a manutenção da higiene bucal infantil.</li>
</ul>
Se seu filho já apresentou sinais de cárie, não hesite em procurar um de nossos dentistas, aqui na <a href="https://www.stardente.com.br/a-clinica/">Stardente</a> você e seu filho(a) recebem o tratamento necessário.";s:10:"post_title";s:75:"Higiene Bucal Infantil: Mantenha os Dentes dos Seus Filhos Longe da Cárie.";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:72:"higiene-bucal-infantil-mantenha-os-dentes-dos-seus-filhos-longe-da-carie";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-07-10 16:52:59";s:17:"post_modified_gmt";s:19:"2019-07-10 19:52:59";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:77:"http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/stardente/?p=140";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}