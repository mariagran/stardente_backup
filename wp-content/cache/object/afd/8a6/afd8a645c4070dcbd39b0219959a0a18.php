�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"128";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-23 11:31:22";s:13:"post_date_gmt";s:19:"2017-11-23 13:31:22";s:12:"post_content";s:3478:"A sensibilidade nos dentes atinge 1 em cada 4 pessoas no Brasil. Se você já não consegue mais tomar sorvete com tanta facilidade, sente dor nos dentes sempre que toma algum liquido gelado ou quando come algum alimento ácido, pode fazer parte deste grupo.
<h3><strong>Quais as causas da sensibilidade nos dentes?</strong></h3>
As causas mais comuns da sensibilidade dentária são o <strong>enfraquecimento do esmalte dentário </strong>e a <strong>retração gengival</strong> - a retração gengival pode ser consequência da <a href="https://www.stardente.com.br/gengivite-pode-colocar-sua-saude-bucal-em-perigo/">gengivite</a>, que facilita a retração pela irritação da gengiva.

O excesso de escovação também pode ser a causa da sensibilidade - muitas pessoas pensam que para uma limpeza eficaz dos dentes, língua e boca é necessário usar a força para escovar e, combinando isso com uma escova de cerdas duras só torna o esmalte dental ainda mais sensível.
<h3><strong>Posso fazer o tratamento da sensibilidade dental em casa?</strong></h3>
Sim, mas só depois do aval do dentista. A consulta é absolutamente necessária para que haja certeza que o problema é somente a sensibilidade e, principalmente para diagnosticar a causa do problema.

Por exemplo, sua sensibilidade por ser consequência da gengivite e, como já falamos antes aqui, se não for devidamente tratada ela pode evoluir para a periodontite, problema muito mais sério.

Não procrastine, consultas regulares no dentista se fazem absolutamente necessárias para a saúde do seu sorriso.
<h3><strong>O que posso usar para amenizar a sensibilidade nos dentes?</strong></h3>
Existem diversas maneiras de tornar sua alimentação menos dolorida e, o flúor é um dos maiores aliados neste procedimento - Sua aplicação em gel ajuda a fortalecer o esmalte dental, mas esse é um processo gradativo e pode demorar um pouco para surtir efeito.

Existem aplicações em consultórios odontológicos deste gel de flúor, que pode ser feito com moldes de 3 a 5 min. Preste atenção durante a escovação também, use uma escova de cerdas macias e sem aplicar força excessiva - lembrando que uma boa escovação deve levar até 10 min.

Faça bochechos com enxaguantes bucais que contenham flúor em sua fórmula e evite, pelo menos no início do tratamento, alimentos que sensibilizam ainda mais seus dentes.
<h3><strong>O que devo evitar para reduzir a sensibilidade dental?</strong></h3>
Muitas vezes com a sensibilidade incomoda tanto que tudo que queremos é o alívio imediato e, aí começa o perigo.

A internet é repleta de receitas caseiras para aliviar, receitas regadas a bicarbonato de sódio e demais ingredientes comuns do dia a dia. Tome cuidado, muitos desses produtos se usados em excesso ou de forma errônea podem agravar ainda mais o estado do esmalte e pior sua situação.

<a href="https://www.stardente.com.br/tratamentos/clareamento-dental/">Clareamentos dentais</a> também podem deixar seus dentes sensíveis, por isso, cuidado com técnicas caseiras. Antes de qualquer procedimento caseiro consulte um dentista, ele saberá te conduzir ao melhor tratamento.

Se você tem sensibilidades nos dentes, marque uma consulta aqui na Stardente. Temos profissionais preparados para te atender e uma ampla estrutura, com clínicas bem localizadas em Curitiba e em Araucária.

E não esqueça, para um sorriso bonito deve ser saudável, não deixe de lado a higiene diária.";s:10:"post_title";s:57:"A Sensibilidade nos Dentes Não Precisa Mais te Incomodar";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:54:"sensibilidade-nos-dentes-nao-precisa-mais-te-incomodar";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-07-09 17:11:31";s:17:"post_modified_gmt";s:19:"2019-07-09 20:11:31";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:77:"http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/stardente/?p=128";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}