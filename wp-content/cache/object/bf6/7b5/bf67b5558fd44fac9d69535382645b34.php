�d^<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"125";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-23 11:30:36";s:13:"post_date_gmt";s:19:"2017-11-23 13:30:36";s:12:"post_content";s:3107:"As manchas prejudicam sorrisos cada vez mais, são inúmeros agravantes que podem deixar os dentes amarelados.

Alimentos e bebidas proporcionam prazer, mas se não houver uma boa higienização será difícil combater o amarelamento.

Os dentes mudam de pigmentação pela porosidade do esmalte dentário -&nbsp; por mais que pareçam ter a superfície lisa, eles têm poros minúsculos, que em contato com alimentos de alta pigmentação podem alterar a cor por algum tempo e, se não houver a higienização correta esses corantes podem alterar a coloração real dos dentes permanentemente.
<h3><strong>Alimentos que causam manchas nos dentes:</strong></h3>
<strong>Bebidas:</strong>

Vinhos tinto ou rose, sucos de uva, açaí, refrigerantes de cola, café e chá preto, além das demais bebidas com corantes podem manchar os seus dentes.
Já as bebidas efervescentes possuem um teor ácido muito grande, e por isso aumentam mais ainda a possibilidade do surgimento de manchas.

<strong>Alimentos:</strong>

Existem frutas, verduras e legumes, ricas em vitaminas e minerais, mas também ricas em cores fortes, que podem incitar um sorriso amarelo. Dentre elas, podemos citar: beterraba, frutas vermelhas, mirtilo e também os alimentos ácidos, que enfraquecem a proteção natural dos dentes, deixando-os sensíveis, como o limão, laranja e abacaxi.

<strong>Outros hábitos também podem contribuir com os dentes amarelos:</strong>

Ressaltamos também os malefícios do cigarro, não somente a saúde, mas também para o sorriso. O ato de fumar ou mascar tabaco deixa os dentes sensíveis e, suas inúmeras substâncias tóxicas são extremamente maléficas a saúde.

O perigo das manchas nos dentes que determinados alimentos e bebidas podem oferecer ao nosso sorriso é iminente, por isso é muito importante manter sua saúde bucal em dia.

Garantir a escovação de no mínimo 3 vezes ao dia, o uso do fio dental e o como uma forma de amenizar a possibilidade de dentes manchados, faça um bochecho com água logo após o consumo de alimentos com forte pigmentação.

Aguarde em média 30 minutos após a ingestão para escovar os dentes, é importante deixar um tempo para que a saliva reative seu PH e comece sua própria proteção do esmalte dentário. Por isso recomendamos o bochecho com água, porque assim é possível retirar algum corante que tenha restado, sem precisar danificar o esmalte com a escovação precoce.
<h3><strong>Como reverter o amarelamento dos dentes?</strong></h3>
Se você notou o que seus dentes estão mais escuros ultimamente, com algumas manchas e não está satisfeito com seu sorriso, existem algumas alternativas para resgatar seu sorriso - como o uso de cremes dentais clareadores, enxaguantes bucais ou <a href="https://www.stardente.com.br/tratamentos/clareamento-dental/">clareamentos dentais</a> feitos em consultórios, seja por molde ou laser.

Mas não faça nenhum tratamento sem a prescrição médica, venha até a Stardente e consulte com um de nossos profissionais. Com certeza você encontrará a melhor solução para resgatar seu sorriso.";s:10:"post_title";s:60:"Manchas nos Dentes: Alimentos que Podem Amarelar seu Sorriso";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:59:"manchas-nos-dentes-alimentos-que-podem-amarelar-seu-sorriso";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-07-10 16:41:19";s:17:"post_modified_gmt";s:19:"2019-07-10 19:41:19";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:77:"http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/stardente/?p=125";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}