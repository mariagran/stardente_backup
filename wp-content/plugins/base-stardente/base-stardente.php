<?php
/**
 * Plugin Name: Base Stardente
 * Description: Controle base do tema Stardente.
 * Version: 0.1
 * Author: Hudson Caronilo
 * Author URI: 
 * Licence: GPL2
 */

	function baseStardente () {

		//TIPOS DE CONTEÚDO
		conteudosStardente();

		//TAXONOMIA
		taxonomiaStardente();

		//META BOXES
		metaboxesStardente();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosStardente (){

			// TIPOS DE DESTQUE
			tipoDestaque();

			// SERVIÇOS
			tipoServico();

			//PLANOS DE SAÚDE
			tipoPlanoSaude();

			// EQUIPE
			tipoEquipe();



			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE COLABORADOR
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'especialista',
									'singular_name'      => 'especialista',
									'menu_name'          => 'Especialista',
									'name_admin_bar'     => 'especialista',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo membro',
									'new_item'           => 'Novo membro',
									'edit_item'          => 'Editar membro',
									'view_item'          => 'Ver membro',
									'all_items'          => 'Todos os membros',
									'search_items'       => 'Buscar membros',
									'parent_item_colon'  => 'Dos membros',
									'not_found'          => 'Nenhum membro cadastrado.',
									'not_found_in_trash' => 'Nenhum membro na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-nametag',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {

			$rotulosServico = array(
									'name'               => 'Tratamentos',
									'singular_name'      => 'Tratamento',
									'menu_name'          => 'Tratamentos',
									'name_admin_bar'     => 'Tratamentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Tratamento',
									'new_item'           => 'Novo Tratamento',
									'edit_item'          => 'Editar Tratamento',
									'view_item'          => 'Ver Tratamento',
									'all_items'          => 'Todos os Tratamentos',
									'search_items'       => 'Buscar Tratamentos',
									'parent_item_colon'  => 'Dos Tratamentos',
									'not_found'          => 'Nenhum Tratamento cadastrado.',
									'not_found_in_trash' => 'Nenhum Tratamento na lixeira.'
								);

			$argsServico 	= array(
									'labels'             => $rotulosServico,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'tratamentos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('tratamento', $argsServico);

		}


		// CUSTOM POST TYPE PLANO DE SAÚDE
		function tipoPlanoSaude() {

			$rotulosPlanoSaude = array(
									'name'               => 'Planos de saúde',
									'singular_name'      => 'plano de saúde',
									'menu_name'          => 'Planos de saúde',
									'name_admin_bar'     => 'Planos de saúde',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo plano de saúde',
									'new_item'           => 'Novo plano de saúde',
									'edit_item'          => 'Editar plano de saúde',
									'view_item'          => 'Ver plano de saúde',
									'all_items'          => 'Todos os planos de saúde',
									'search_items'       => 'Buscar planos de saúde',
									'parent_item_colon'  => 'Dos planos de saúde',
									'not_found'          => 'Nenhum plano de saúde cadastrado.',
									'not_found_in_trash' => 'Nenhum plano de saúde na lixeira.'
								);

			$argsPlanoSaude 	= array(
									'labels'             => $rotulosPlanoSaude,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'planos-de-saude' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('plano-de-saude', $argsPlanoSaude);

		}

		
		

		
	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesStardente(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Stardente_';

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDestaque',
					'title'			=> 'Detalhes do destaque',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Link do destaque: ',
							'id'    => "{$prefix}destaque_link",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxTratamento',
					'title'			=> 'Detalhes do tratamento',
					'pages' 		=> array('tratamento'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Breve descrição: ',
							'id'    => "{$prefix}tratamento_breve_descricao",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Ícone do tratamento: ',
							'id'    => "{$prefix}tratamento_icone",
							'desc'  => '',
							'type'  => 'image_advanced',
							'max_file_uploads' => 1	
						), 
						array(
							'name'  => 'Galeria de fotos: ',
							'id'    => "{$prefix}tratamento_galeria_Fotos",
							'desc'  => '',
							'type'  => 'image_advanced',
						), 
						
					),
				);

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxEquipe',
					'title'			=> 'Detalhes do integrante',
					'pages' 		=> array('equipe'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'CRO: ',
							'id'    => "{$prefix}equipe_funcao",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);


				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaStardente () {
			taxonomiaTratamentos();
		}

		function taxonomiaTratamentos() {

			$rotulosTratamentos = array(
												'name'              => 'Categorias de tratamento',
												'singular_name'     => 'Categorias de tratamentos',
												'search_items'      => 'Buscar categoria do tratamento',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do tratamento',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias tratamentos',
											);

			$argsTratamentos 		= array(
												'hierarchical'      => true,
												'labels'            => $rotulosTratamentos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'tratamentos' ),
											);

			register_taxonomy( 'categoriaTratamento', array( 'tratamento' ), $argsTratamentos);

		}
		
		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseStardente');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseStardente();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );