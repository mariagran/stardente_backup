<?php
/**
 * Template Name: Quem Somos
 * Description: Quem Somos
 *
 * @package Unika
 */
$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
get_header(); ?>
<div class="pg pg-empresa">
	<!-- ÁREA DE CONTATO -->
	<div class="areaContato">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="select">
						<select name="" class="funcaoSelect">
							<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
							<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
							<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
						</select>
					</div>
					<div class="areaContatoTelefone idEndereco0 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_centro'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_centro'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco1 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_novo_mundo'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_novo_mundo'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco2 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_Araucaria'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_Araucaria'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></a>
					</div>

				</div>
				<div class="col-sm-5 text-right">
					<div class="areaEndereco idEndereco0 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_centro as $doutores_centro):
								$doutores_centro = $doutores_centro;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_centro ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco1 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_novo_mundo as $doutores_novo_mundo):
								$doutores_novo_mundo = $doutores_novo_mundo;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_novo_mundo ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco2 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_araucaria as $doutores_araucaria):
								$doutores_araucaria = $doutores_araucaria;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_araucaria ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<section class="areaQuemSomos" style="background: url(<?php echo $foto ?>)">
	
		<div class="container">
			<div class="paginadorLink">
				<div class="row">
					
					<div class="col-xs-6">
						<a href="<?php echo home_url('/'); ?>">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
							Voltar
						</a>
					</div>

					<div class="col-xs-6 text-right">
						<p>
							<a href="<?php echo home_url('/'); ?>">Home /</a>
							<a href="<?php echo get_the_title()?>"><?php get_the_title() ?>A Clínica </a>
						</p>
					</div>

				</div>
			</div>
		</div>

		<div class="container">
			<h6><?php echo get_the_title() ?></h6>
			<?php echo the_content() ?>
			<a href="http://www.stardente.com.br/tratamentos/restauracao/" class="link">Tratamentos</a>
		</div>
	</section>


	<!-- SESSÃO DE PLANO DE SAÚDE -->
	<div class="container">
		<section class="sessaoPlanosSaude">
			<h6>Planos de Saúde</h6>
			
			<div id="carrosselPlanosSaude" class="owl-Carousel">
				<?php 
					//LOOP DE POST DESTAQUES
					$planosSaude = new WP_Query( array( 'post_type' => 'plano-de-saude', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $planosSaude->have_posts() ) : $planosSaude->the_post();
						$fotoplanosSaude = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoplanosSaude = $fotoplanosSaude[0];
					
				 ?>
				<figure class="item">
					<img src="<?php echo $fotoplanosSaude  ?>" class="hvr-push" alt="<?php echo get_the_title() ?>">
				</figure>
				<?php endwhile; wp_reset_query(); ?>
			</div>

		</section>
	</div>
	
	<!-- SESSÃO NOSSAS SEDES -->
	<div class="background">
		<div class="container">
			<section class="sessaoNossasSedes">
				<h6>Nossas Sedes</h6>
				<div class="nossassedes">
				<div class="row">
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap1" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.260016108764!2d-49.28044958498583!3d-25.429569183788548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40c6c36abc9%3A0x36b913a9fc26ec33!2sAlameda+Cabral%2C+407+-+S%C3%A3o+Francisco%2C+Curitiba+-+PR%2C+82590-300!5e0!3m2!1spt-BR!2sbr!4v1560884619984!5m2!1spt-BR!2sbr" frameborder="0" style="border:0" allowfullscreen=""></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_centro_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_centro'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_centro'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap2" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3601.3119714901623!2d-49.28431178557293!3d-25.494640083758775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfb5519f9e4a9%3A0xf5dacca9982cef75!2sAv.+Bras%C3%ADlia%2C+6250+-+Cap%C3%A3o+Raso%2C+Curitiba+-+PR%2C+81020-010!5e0!3m2!1spt-BR!2sbr!4v1548700359398"  frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_novo_mundo'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_novo_mundo'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap3" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3598.336043097338!2d-49.40562578557038!3d-25.593747483713283!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dd02350a0e73cb%3A0xf3ad1e77950bc549!2sR.+Paulo+Alves+Pinto%2C+172+-+Centro%2C+Arauc%C3%A1ria+-+PR%2C+83704-370!5e0!3m2!1spt-BR!2sbr!4v1548700391662"  frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_Araucaria'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_Araucaria'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>

	<!-- ÁREA LINK PARA AGENDAMENTO -->
	<?php if ($configuracao['opt_form_titulo_d']): ?>
	<div class="areaAgendar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="area segundaopcao">
						<p><?php echo  $configuracao['opt_form_titulo_d'] ?></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="area text-right segundaopcao">
						<?php if ($configuracao['opt_form_btn_b']): ?>
						<a href="<?php echo  $configuracao['opt_form_btn_b'] ?>">Agendar</a>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<?php 
		//LOOP DE POST EQUIPE
		$integrantes = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) ); 
		
		if (empty($integrantes)):
			
		
	?>
	<section class="areaEspecialista">
		<h6>Dentistas Stardente</h6>
		<div class="container">
			<div id="carrosseldr" class="owl-Carousel">
				<?php 
					//LOOP DE POST EQUIPE
					$integrantes = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $integrantes->have_posts() ) : $integrantes->the_post();
						$fotoIntegrante = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoIntegrante = $fotoIntegrante[0];
						$equipe_funcao = rwmb_meta('Stardente_equipe_funcao');

				?>

				<div  class="item" title="<?php echo get_the_title() ?> ">
					<figure style="background:url(<?php echo $fotoIntegrante ?>)"></figure>
					<h3><?php echo get_the_title() ?></h3>
					<p><?php echo $equipe_funcao ?></p>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
		<div class="btn">
			<button id="carrosseldrLeft"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
			<button id="carrosseldrRight"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
		</div>
	</section>
	<?php endif; ?>
	
</div>

<?php get_footer(); ?>