<?php
/**
 * Template Name: Página de Tratamentos
 * Description: Página de Tratamentos
 *
 * @package Stardente
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
$titulo = get_the_title();
$galeria = rwmb_meta('Stardente_tratamento_galeria_Fotos');
$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);
get_header(); ?>

<!-- PÁGINA DE TRATAMENTOS -->
<div class="pg pg-tratamentos">
	<!-- ÁREA DE CONTATO -->
	<div class="areaContato">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="select">
						<select name="" class="funcaoSelect">
							<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
							<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
							<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
						</select>
					</div>
					<div class="areaContatoTelefone idEndereco0 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_centro'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_centro'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco1 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_novo_mundo'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_novo_mundo'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco2 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_Araucaria'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_Araucaria'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></a>
					</div>

				</div>
				<div class="col-sm-5 text-right">
					<div class="areaEndereco idEndereco0 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_centro as $doutores_centro):
								$doutores_centro = $doutores_centro;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_centro ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco1 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_novo_mundo as $doutores_novo_mundo):
								$doutores_novo_mundo = $doutores_novo_mundo;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_novo_mundo ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco2 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_araucaria as $doutores_araucaria):
								$doutores_araucaria = $doutores_araucaria;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_araucaria ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="paginadorLink">
		<a href="<?php echo home_url('/'); ?>">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
			Voltar
		</a>
		<p>
			<a href="<?php echo home_url('/'); ?>">Home /</a>
			<a href="<?php echo get_the_title()?>"><?php get_the_title() ?>Tratamentos /</a>
			<a href="<?php echo get_permalink()?>"><?php echo get_the_title() ?></a>
		</p>
		
	</div>

	<div class="container">
		<figure class="banner" style="background: url(<?php echo $foto ?>)"></figure>

		<section class="tramentos">
			<h6 class="hidden"><?php echo get_the_title() ?></h6>

			<div class="row">
				<div class="col-sm-3 tratamentosSidebar">

					<aside>
						<button id="tratamentosClose">X</button>
						<?php
							$i= 0;
							$postTratamentos = new WP_Query( array( 
								'post_type' => 'tratamento', 
								'orderby' => 'id', 
								'order' => 'desc', 
								'posts_per_page' => -1,
								// FILTRANDO PELA CATEGORIA DESTAQUE
								'tax_query'     => array(
									array(
										// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
										'taxonomy' => 'categoriaTratamento',
										// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
										'field'    => 'name',
										// SLUG DA CATEGORIA
										'terms'    => get_the_title(),
										)
									)
							) 
						);
							while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();
							if (get_the_title() == $titulo):
					 	?>
						
						<a href="<?php echo get_permalink() ?>"><h3 class="ativo"><?php echo get_the_title() ?> <i class="fa fa-caret-right" aria-hidden="true"></i></h3></a>

						<?php else: ?>
							
							<a href="<?php echo get_permalink() ?>"><h3><?php echo get_the_title() ?> <i class="fa fa-caret-right" aria-hidden="true"></i></h3></a>
						
						<?php endif;endwhile; wp_reset_query(); ?>
					</aside>
				</div>
				<div class="col-sm-9">
					<button id="verTratamentos" class="btnVertratamentos">Ver tratamentos +</button>
					<article>
						<h1><?php echo get_the_title() ?></h1>
						<?php echo the_content() ?>
					</article>

					<?php if ($galeria):?>
					<div class="galeriaImagens">
						<h6 class="hidden">Galeria de Imagens</h6>

						<div class="btnCarrossel">
							<button id="carrosselFotosGaleriaLeft"><i class="fa fa-caret-left" aria-hidden="true"></i></button>
							<button id="carrosselFotosGaleriaRight"><i class="fa fa-caret-right" aria-hidden="true"></i></button>
						</div>

						<div id="carrosselFotosGaleria" class="owl-Carousel">
							<?php foreach ($galeria as $galeria): $galeria = $galeria['full_url']; ?>
							<figure class="item">
							<a href="<?php echo $galeria ?>" id="fancy" rel="gallery1">
								<img src="<?php echo $galeria ?>" alt="<?php echo  get_the_title() ?>">
							</a>
							</figure>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>

			<!-- <div class="paginador">
				<div class="row">
					

					<div class="col-xs-6">
						<?php next_post('%','<i class="fa fa-angle-left" aria-hidden="true"></i>') ?>
					</div>
					<div class="col-xs-6 text-right">
						
						<?php previous_post('%','<i class="fa fa-angle-right" aria-hidden="true"></i>') ?>
					</div>
				</div>
			</div> -->
		</section>
	</div>

	<!-- ÁREA LINK PARA AGENDAMENTO -->
	<?php if ($configuracao['opt_form_titulo_d']): ?>
	<div class="areaAgendar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="area segundaopcao">
						<p><?php echo  $configuracao['opt_form_titulo_d'] ?></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="area text-right segundaopcao">
						<?php if ($configuracao['opt_form_btn_b']): ?>
						<a href="<?php echo  $configuracao['opt_form_btn_b'] ?>">Agendar</a>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>


<?php get_footer(); ?>