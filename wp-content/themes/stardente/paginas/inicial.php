<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package Stardente
 */

$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);

get_header(); ?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
	
	<!-- ÁREA DE CONTATO -->
	<div class="areaContato">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="select">
						<select name="" class="funcaoSelect">
							<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
							<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
							<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
						</select>
					</div>
					<div class="areaContatoTelefone idEndereco0 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_centro'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_centro'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco1 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_novo_mundo'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_novo_mundo'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco2 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_Araucaria'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_Araucaria'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></a>
					</div>

				</div>
				<div class="col-sm-5 text-right">
					<div class="areaEndereco idEndereco0 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_centro as $doutores_centro):
								$doutores_centro = $doutores_centro;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_centro ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco1 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_novo_mundo as $doutores_novo_mundo):
								$doutores_novo_mundo = $doutores_novo_mundo;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_novo_mundo ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco2 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_araucaria as $doutores_araucaria):
								$doutores_araucaria = $doutores_araucaria;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_araucaria ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- SESSÃO DESTAQUE -->
	<section class="sessaoDestaque">
		<h6 class="hidden">Destaque</h6>
		
		<div id="carrosselDestaque" class="owl-Carousel">
			<?php 
				//LOOP DE POST DESTAQUES
				$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
				while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
					$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoDestaque = $fotoDestaque[0];
			 ?>
			<!-- ITEM CARROSSEL -->
			<a href="<?php echo $destaque_link = rwmb_meta('Stardente_destaque_link'); ?> " class="item" style="background: url(<?php echo $fotoDestaque ?>)">
				<div class="container">
					<div class="textoDestaque">
						<?php echo the_content() ?>
						<?php if ($destaque_link = rwmb_meta('Stardente_destaque_link')): ?>
							<span>Agendar</span>
						<?php endif; ?>
					</div>
				</div>		
			</a>
			<?php endwhile; wp_reset_query(); ?>
		</div>
		<div class="btnCarrossel">
			<button id="btnLeft"></button>
			<button id="btnRight"></button>
		</div>
	</section>

	<!-- SESSÃO DE PLANO DE SAÚDE -->
	
	<section class="sessaoPlanosSaude">
	
		<h1>Conheça a Clínica Odontológica Stardente</h1>
		<article>
			<p>A clínica Stardente, há mais de 10 anos no mercado conta com uma equipe de dentistas em Curitiba e em Araucária realizando todos os procedimentos, desde restaurações, próteses e implantes, até a queridinha dos famosos, lentes de contato dental.</p>
		</article>
		<div class="row areaPlanosOdontologicos">
			<div class="col-sm-5 tituloPlanos">
				<p>Atendemos <br> Planos Odontológicos</p>
			</div>
			<div class="col-sm-7">
				<div class="logosAtendimento">
					<?php 
					//LOOP DE POST DESTAQUES
					$planosSaude = new WP_Query( array( 'post_type' => 'plano-de-saude', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $planosSaude->have_posts() ) : $planosSaude->the_post();
						$fotoplanosSaude = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoplanosSaude = $fotoplanosSaude[0];
						
						?>
						<figure class="item">
							<img src="<?php echo $fotoplanosSaude  ?>" class="hvr-push" tilte="<?php echo get_the_title() ?>" alt="<?php echo get_the_title() ?>">
						</figure>
					<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</section>

	<!-- SESSÃO AGENDAMENTO DE CONSULTA -->
	<div class="container">
		<section class="sessaoAgendamento">
			<h6 class="hidden">Agende sua Consulta</h6>
			<div class="row">
				<div class="col-sm-12">
					<div class="formAgendamento">
						<p>Dentistas Curitiba e Araucária - Agende sua Consulta</p>

						<div class="form">
							<?php echo do_shortcode('[contact-form-7 id="105" title="Formulário de agendamento página inicial"]'); ?>
						</div>
					</div>
				</div>
				
			</div>
		</section>
	</div>

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="sessaoServicos">
		<h2>Tratamentos Clínica Odontológica Stardente</h2>
		<div class="container">
		
			<div class="carroselServicos">
				<div class="item">
					<?php 
						
						//LOOP DE POST DESTAQUES
						$postTratamentos = new WP_Query( array( 'post_type' => 'tratamento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 8) );
						while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();
							$fotopostTratamentos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotopostTratamentos = $fotopostTratamentos[0];
							$tratamento_icone = rwmb_meta('Stardente_tratamento_icone');
							
					 ?>
					<div class="areaServico">
						<a href="<?php echo get_permalink() ?> ">	
							<?php 
								foreach ($tratamento_icone as $tratamento_icone):
									$tratamento_icone = $tratamento_icone['full_url'];
							
							?>						
							<img src="<?php echo $tratamento_icone ?>" title="<?php echo get_the_title() ?>"  alt="<?php echo get_the_title() ?> ">
							<?php endforeach; ?>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo $descricao = rwmb_meta('Stardente_tratamento_breve_descricao'); ?></p>
						</a>
					</div>
					<?php endwhile; wp_reset_query(); ?>
					
				</div>
			</div>
		</div>
	</section>
	
	<!-- ÁREA LINK PARA AGENDAMENTO -->
	<?php if ($configuracao['opt_form_titulo_c']):?>
	<div class="areaAgendar">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="area">
						<p><?php echo  $configuracao['opt_form_titulo_c'] ?></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="area text-right">
					<?php if ($configuracao['opt_form_btn_a']): ?>
						<a href="<?php echo  $configuracao['opt_form_btn_a'] ?>"> Agende sua Consulta com Dentista </a>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="container">
		<section class="descriptionTratamento">
			<h3>Tratamento Estéticos</h3>
				
			<div class="row">
				<div class="col-sm-7">
					<article class="tratamentoDescription">
						<?php echo the_content(); ?>
					</article>
				</div>
				<div class="col-sm-5">
					<aside>
						<?php 
							$postTratamentos = new WP_Query( array( 
								'post_type' => 'tratamento', 
								'orderby' => 'id', 
								'order' => 'desc', 
								'posts_per_page' => -1,
								// FILTRANDO PELA CATEGORIA DESTAQUE
								'tax_query'     => array(
									array(
										// TIPO DE CATEGORIA A SER FILTRADA / CATEGORIA DE POST
										'taxonomy' => 'categoriaTratamento',
										// PASSANDO O ATRIBUTO SLUG PARA A FILTRAGEM DO POST// PODE SER id 
										'field'    => 'name',
										// SLUG DA CATEGORIA
										'terms'    => "Estetica",
										)
									)
								) 
							);
							//LOOP DE POST DESTAQUES
							
							while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();
						 ?>
						<a href="<?php echo get_permalink() ?> "><?php echo get_the_title() ?></a>
						<?php endwhile; wp_reset_query(); ?>
					</aside>
				</div>
			</div>

		</section>
	</div>

	<!-- SESSÃO NOSSAS SEDES -->
	<div class="container">
		<section class="sessaoNossasSedes">
			<h6>Dentistas em Curitiba e Araucária</h6>

			<div class="nossassedes">
				<div class="row">
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap1" class="mapa">
							    
								<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_centro'] ?>" target="_blank" style="margin-top:0;margin-bottom:0;">
									<img style="display:block;width:100%;height:auto;" src="https://www.stardente.com.br/wp-content/uploads/2019/06/mapa1.jpg">
								</a>
								<
							</div>
							<h5><?php echo $configuracao['opt_telefone_centro_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_centro'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_centro'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap2" class="mapa">
								<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_novo_mundo'] ?>" target="_blank" style="margin-top:0;margin-bottom:0;">
								<img style="display:block;width:100%;height:auto;" src="https://www.stardente.com.br/wp-content/uploads/2019/06/2.jpg">
							</a>
							</div>
							<h5><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_novo_mundo'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_novo_mundo'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap3" class="mapa">
								<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_Araucaria'] ?>" target="_blank" style="margin-top:0;margin-bottom:0;">
								<img style="display:block;width:100%;height:auto;" src="https://www.stardente.com.br/wp-content/uploads/2019/06/3.jpg">
							</a>
							</div>
							<h5><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_Araucaria'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_Araucaria'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<!-- SESSÃO BLOG -->
	<section class="sessaoBlog">
		<div class="container">
			<h6>Blog Stardente</h6>

			<div class="blogPost">
			
				<div class="row owl-Carousel" id="carrosselBlog">
					<?php 
						//LOOP DE POST DESTAQUES
						$posBlog = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
						while ( $posBlog->have_posts() ) : $posBlog->the_post();
							$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$fotoPost = $fotoPost[0];
					 ?>
					<div class="col-sm-4 item">

						<a href="<?php echo get_permalink() ?>">
							<figure style="background: url( <?php echo $fotoPost ?>)"></figure>
							<h3><?php echo get_the_title() ?></h3>
							<p><?php customExcerpt(100); ?></p>
							<span>Leia mais</span>
						</a>
					</div>
					<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</section>
	

	<!-- ÁREA LINK PARA AGENDAMENTO -->
	<?php if (false): ?>
	<div class="areaAgendar">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="area segundaopcao">
						<p><?php echo  $configuracao['opt_form_titulo_d'] ?></p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="area text-right segundaopcao">
						<?php if ($configuracao['opt_form_btn_b']): ?>
						<a href="<?php echo  $configuracao['opt_form_btn_b'] ?>">Agendar</a>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

</div>
<script type="text/javascript">
	// $(function() {
	// 	new Maplace({
	// 		show_markers: true,
	// 		map_div: '#gmap1',
	// 		locations: [{
	// 			lat: -25.43611,
	// 			lon: -49.2721383,
	// 			zoom: 15
	// 		}]
	// 	}).Load();
	// 	new Maplace({
	// 		show_markers: true,
	// 		map_div: '#gmap2',
	// 		locations: [{
	// 			lat: -25.4946401,
	// 			lon: -49.2821231,
	// 			zoom: 15
	// 		}]
	// 	}).Load();
	// 	new Maplace({
	// 		show_markers: true,
	// 		map_div: '#gmap3',
	// 		locations: [{
	// 			lat: -25.5937475,
	// 			lon: -49.4056258,
	// 			zoom: 15
	// 		}]
	// 	}).Load();			
	// });
</script>	
<?php get_footer(); ?>