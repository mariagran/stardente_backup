<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package Stardente
 */
$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);

get_header(); ?>
<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
	
	<!-- ÁREA DE CONTATO -->
	<div class="areaContato">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<div class="select">
						<select name="" class="funcaoSelect">
							<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
							<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
							<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
						</select>
					</div>
					<div class="areaContatoTelefone idEndereco0 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_centro'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_centro'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco1 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_novo_mundo'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_novo_mundo'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></a>
					</div>

					<div class="areaContatoTelefone idEndereco2 hiddeInfosTopo">
						<a href="tel:<?php echo $configuracao['opt_telefone_Araucaria'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></a>
						<a href="tel:<?php echo $configuracao['opt_whatsapp_Araucaria'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></a>
					</div>

				</div>
				<div class="col-sm-5 text-right">
					<div class="areaEndereco idEndereco0 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_centro as $doutores_centro):
								$doutores_centro = $doutores_centro;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_centro ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco1 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_novo_mundo as $doutores_novo_mundo):
								$doutores_novo_mundo = $doutores_novo_mundo;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_novo_mundo ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>

					<div class="areaEndereco idEndereco2 hiddeInfosTopo">
						<div class="row">
							<?php foreach ($doutores_araucaria as $doutores_araucaria):
								$doutores_araucaria = $doutores_araucaria;
							?>
							<div class="col-xs-6">
								<span><?php echo $doutores_araucaria ?></span>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="paginadorLink">
		<a href="<?php echo home_url('/'); ?>">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
			Voltar
		</a>
		<p>
			<a href="<?php echo home_url('/'); ?>">Home /</a>
			<a href="<?php echo home_url('/contato/'); ?>">Contato</a>
		</p>
		
	</div>
	<section class="sessaoContato">
		<h6 class="hidden">Contato</h6>
		<div class="container">
			<div class="form">
				<?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato 1"]'); ?>
			</div>
		</div>
	</section>

	<!-- SESSÃO NOSSAS SEDES -->
	<div class="background">
		<!-- SESSÃO NOSSAS SEDES -->
		<div class="container">
			<section class="sessaoNossasSedes">
				<h6>Nossas Sedes</h6>

				<div class="nossassedes">
				<div class="row">
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap1" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3603.260016108764!2d-49.28044958498583!3d-25.429569183788548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40c6c36abc9%3A0x36b913a9fc26ec33!2sAlameda+Cabral%2C+407+-+S%C3%A3o+Francisco%2C+Curitiba+-+PR%2C+82590-300!5e0!3m2!1spt-BR!2sbr!4v1560884619984!5m2!1spt-BR!2sbr" frameborder="0" style="border:0" allowfullscreen=""></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_centro_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_centro'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_centro'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap2" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3601.3119714901623!2d-49.28431178557293!3d-25.494640083758775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dcfb5519f9e4a9%3A0xf5dacca9982cef75!2sAv.+Bras%C3%ADlia%2C+6250+-+Cap%C3%A3o+Raso%2C+Curitiba+-+PR%2C+81020-010!5e0!3m2!1spt-BR!2sbr!4v1548700359398"  frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_novo_mundo'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_novo_mundo'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="enderecos">
							<div id="gmap3" class="mapa">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3598.336043097338!2d-49.40562578557038!3d-25.593747483713283!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dd02350a0e73cb%3A0xf3ad1e77950bc549!2sR.+Paulo+Alves+Pinto%2C+172+-+Centro%2C+Arauc%C3%A1ria+-+PR%2C+83704-370!5e0!3m2!1spt-BR!2sbr!4v1548700391662"  frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<h5><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></h5>
							<p><?php echo $configuracao['opt_endereco_Araucaria'] ?></p>
							<strong><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?> </strong>
							<strong><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?> </strong>
							<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_Araucaria'] ?>" target="_blank">Como chegar - Ver no mapa</a>
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>

</div>	

<?php get_footer(); ?>
