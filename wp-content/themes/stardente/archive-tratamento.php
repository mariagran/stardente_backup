<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stardente
 */
$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);
get_header(); ?>
<!-- PÁGINA BLOG  -->
	<div class="pg pg-blog">

		<!-- ÁREA DE CONTATO -->
		<div class="areaContato">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="select">
							<select name="" class="funcaoSelect">
								<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
								<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
								<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
							</select>
						</div>
						<div class="areaContatoTelefone idEndereco0 hiddeInfosTopo">
							<a href="tel:<?php echo $configuracao['opt_telefone_centro'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></a>
							<a href="tel:<?php echo $configuracao['opt_whatsapp_centro'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></a>
						</div>

						<div class="areaContatoTelefone idEndereco1 hiddeInfosTopo">
							<a href="tel:<?php echo $configuracao['opt_telefone_novo_mundo'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></a>
							<a href="tel:<?php echo $configuracao['opt_whatsapp_novo_mundo'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></a>
						</div>

						<div class="areaContatoTelefone idEndereco2 hiddeInfosTopo">
							<a href="tel:<?php echo $configuracao['opt_telefone_Araucaria'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></a>
							<a href="tel:<?php echo $configuracao['opt_whatsapp_Araucaria'] ?>"><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></a>
						</div>

					</div>
					<div class="col-sm-5 text-right">
						<div class="areaEndereco idEndereco0 hiddeInfosTopo">
							<div class="row">
								<?php foreach ($doutores_centro as $doutores_centro):
									$doutores_centro = $doutores_centro;
								?>
								<div class="col-xs-6">
									<span><?php echo $doutores_centro ?></span>
								</div>
								<?php endforeach; ?>
							</div>
						</div>

						<div class="areaEndereco idEndereco1 hiddeInfosTopo">
							<div class="row">
								<?php foreach ($doutores_novo_mundo as $doutores_novo_mundo):
									$doutores_novo_mundo = $doutores_novo_mundo;
								?>
								<div class="col-xs-6">
									<span><?php echo $doutores_novo_mundo ?></span>
								</div>
								<?php endforeach; ?>
							</div>
						</div>

						<div class="areaEndereco idEndereco2 hiddeInfosTopo">
							<div class="row">
								<?php foreach ($doutores_araucaria as $doutores_araucaria):
									$doutores_araucaria = $doutores_araucaria;
								?>
								<div class="col-xs-6">
									<span><?php echo $doutores_araucaria ?></span>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<section class="sessaoPost">
				<h6 class="hidden">Blog</h6>
				
				<?php 
									
					if ( have_posts() ) :while ( have_posts() ) : the_post();
					$fotoBlog = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoBlog = $fotoBlog[0];
					global $post;
					$categories = get_the_category();
				?>
				<a href="<?php echo get_permalink() ?>" class="post">
					<figure style="background: url(<?php echo $fotoBlog  ?>)"></figure>
					<h2><?php echo get_the_title() ?></h2>
					<span><?php the_time('j M Y | g:i a ') ?> </span>
					<p><?php customExcerpt(250); ?></p>
					<small>Continuar lendo</small>
				</a>
				<?php endwhile;endif; wp_reset_query(); ?>
			</section>

			<section class="sessaoPost hidden">
				<figure style="background: url(img/fotoDestaque.jpg)"></figure>
				<article>
					<h6>Ortodontia</h6>
				</article>
			</section>
		</div>

		<!-- ÁREA LINK PARA AGENDAMENTO -->
		<?php if ($configuracao['opt_form_titulo_d']): ?>
		<div class="areaAgendar">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="area segundaopcao">
							<p><?php echo  $configuracao['opt_form_titulo_d'] ?></p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="area text-right segundaopcao">
							<?php if ($configuracao['opt_form_btn_b']): ?>
							<a href="<?php echo  $configuracao['opt_form_btn_b'] ?>">Agendar</a>
						<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>	

<?php
get_sidebar();
get_footer();
