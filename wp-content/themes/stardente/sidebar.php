<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stardente
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="col-sm-3">
	<aside>
		<a href="#"><h3 class="ativo">ortodontia <i class="fa fa-caret-right" aria-hidden="true"></i></h3></a>
		<?php
			
			$postTratamentos = new WP_Query( array( 'post_type' => 'tratamento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
			while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();

				
	 	?>
		<a href="<?php get_the_title() ?>"><h3><?php echo get_the_title() ?> <i class="fa fa-caret-right" aria-hidden="true"></i></h3></a>
		<?php endwhile; wp_reset_query(); ?>
	</aside>
</div>