<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stardente
 */

get_header(); ?>
<div class="pg pg-empresa">
	
	<section class="areaQuemSomos" style="background: url(<?php echo $foto ?>)">
	

		<div class="container">
			<h6><?php echo get_the_title() ?></h6>
			<?php echo the_content() ?>
		</div>
	</section>
</div>

<?php

get_footer();
