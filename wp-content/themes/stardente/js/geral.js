	
$(function(){
	
	$(".navbar-collapse ul").addClass('nav navbar-nav');
	$("#collapse > div").removeClass('nav navbar-nav');

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {

		//CARROSSEL DE DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,	   
	        autoplay:true,    
		    autoplayTimeout:3000,
		    autoplayHoverPause:true,
		    scroll:true,
		    navigation: true,
		    smartSpeed: 450,
		});
		var carrosselDestaque = $("#carrosselDestaque").data('owlCarousel');
		$('#btnLeft').click(function(){ carrosselDestaque.prev(); });
		$('#btnRight').click(function(){ carrosselDestaque.next(); });

		if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {


			//CARROSSEL DE DESTAQUE
			$("#carrosselPlanodeSaudo").owlCarousel({
				items : 1,
				dots: true,
				loop: true,
				lazyLoad: true,
				mouseDrag:true,
				touchDrag  : true,	   
				autoplay:true,    
				autoplayTimeout:3000,
				autoplayHoverPause:true,
				scroll:true,
				navigation: true,
				smartSpeed: 450,

			});
			var carrosselPlanodeSaudo = $("#carrosselPlanodeSaudo").data('owlCarousel');
			$('#btnLeftCarrosselServicos').click(function(){ carrosselPlanodeSaudo.prev(); });
			$('#btnRightCarrosselServicos').click(function(){ carrosselPlanodeSaudo.next(); });
		}
		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			setTimeout(function(){ 
				$(".telefoneLigar a").addClass("linkopen");

			}, 1000);

			//CARROSSEL DE DESTAQUE
			$("#carrosselBlog").owlCarousel({
				items : 1,
				dots: true,
				loop: true,
				lazyLoad: true,
				mouseDrag:true,
				touchDrag  : true,	   
				autoplay:true,    
				autoplayTimeout:3000,
				autoplayHoverPause:true,
				scroll:true,
				navigation: true,
				smartSpeed: 450,

			});
		}
		
		$("#carrosselPlanosSaude").owlCarousel({
			items : 3,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            400:{
			                items:2
			            },
			            600:{
			                items:3
			            },
			           
			            991:{
			                items:3
			            },
			        }	
		});

		$("#carrosseldr").owlCarousel({
			items : 3,
			dots: true,
			loop: true,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            400:{
			                items:2
			            },
			            600:{
			                items:4
			            },
			           
			            991:{
			                items:4
			            },
			        }	
		});
		var carrosseldr = $("#carrosseldr").data('owlCarousel');
		$('#carrosseldrLeft').click(function(){ carrosseldr.prev(); });
		$('#carrosseldrRight').click(function(){ carrosseldr.next(); });
		

	
		$("#carrosselFotosGaleria").owlCarousel({
			items : 5,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:4
			            },
			           
			            991:{
			                items:5
			            },
			            1024:{
			                items:5
			            },
			            1440:{
			                items:5
			            },
			            			            
			        }	
		});
		var carrosselFotosGaleria = $("#carrosselFotosGaleria").data('owlCarousel');
		$('#carrosselFotosGaleriaLeft').click(function(){ carrosselFotosGaleria.prev(); });
		$('#carrosselFotosGaleriaRight').click(function(){ carrosselFotosGaleria.next(); });
		
	});

	// FUNÇÃO DE SELEÇÃO SELECT TOPO
	$(".areaContato .funcaoSelect").change(function(e) {
		var valor1 = "";
		$(".areaContato .funcaoSelect option:selected").each(function(e) {
			$(".hiddeInfosTopo").hide();
			valor1 += $(this).val() + " ";
		});
		$(".idEndereco"+valor1).css({"display":"inline-block"});
	})
	.trigger( "change" );

	// FUNÇÃO DE SELEÇÃO SELECT PÁGINA INICIAL QUADRO - AZUL
	$(".formAgendamento .funcaoSelect").change(function(e) {
		var valor = "";
		$(".formAgendamento .funcaoSelect option:selected").each(function(e) {
			$(".hiddeInfos").hide();
			valor += $(this).val() + " ";
		});
		$(".enderecosContato"+valor).show(200);
	})
	.trigger( "change" );

	// FUNÇÃO DE SELEÇÃO SELECT PÁGINA INICIAL QUADRO - AZUL
	$(".mapaSite .funcaoSelect").change(function(e) {
		var valor = "";
		$(".mapaSite .funcaoSelect option:selected").each(function(e) {
			$(".hiddeInfosFooter").hide();
			valor += $(this).val() + " ";
		});
		$(".enderecosContatoRodape"+valor).show(300);
	})
	.trigger( "change" );
	
	// FUNÇÃO MOSTRAR SEVIÇOS 
    $(window).scroll( function(){
       
        $('.positionLeft').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInLeft animated" );
            }
        });
       
        $('.positionRight').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInRight animated" );
            }
        });
    });
	
	$("a#fancy").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});


	 $("#verTratamentos").click(function(e){
        $(".tratamentosSidebar").addClass("tratamentoOpen");
    }); 
	  $("#tratamentosClose").click(function(e){
        $(".tratamentosSidebar").removeClass("tratamentoOpen");
    }); 
});
