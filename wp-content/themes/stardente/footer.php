<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stardente
 */
global $configuracao;
?>

<!-- RODAPÉ -->
<footer class="rodape">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="mapaSite">
					<strong>Tratamentos</strong>
					<?php
						//LOOP DE POST DESTAQUES
						$postTratamentos = new WP_Query( array( 'post_type' => 'tratamento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 9) );
						while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();
				 	?>
					<a href=" <?php echo get_permalink() ?> "><?php echo get_the_title() ?></a>
					<?php $i++;endwhile; wp_reset_query(); ?>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="mapaSite">
					<strong class="visible">Tratamentos</strong>
					<?php
						$i = 0;
						//LOOP DE POST DESTAQUES
						$postTratamentos = new WP_Query( array( 'post_type' => 'tratamento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
						while ( $postTratamentos->have_posts() ) : $postTratamentos->the_post();

							if ($i > 9):
				 	?>
					<a href=" <?php echo get_permalink() ?> "><?php echo get_the_title() ?></a>
					<?php endif;$i++;endwhile; wp_reset_query(); ?>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="mapaSite">
					<strong>Contato</strong>
					<select name="" id="" class="funcaoSelect">
						<option selected="selected" value="0"><?php echo $configuracao['opt_telefone_centro_titulo'] ?></option>
						<option value="1"><?php echo $configuracao['opt_telefone_novo_mundo_titulo'] ?></option>
						<option value="2"><?php echo $configuracao['opt_telefone_Araucaria_titulo'] ?></option>
					</select>
					


					<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_centro'] ?>" target="_blank" class="enderecosContatoRodape0 hiddeInfosFooter displayNone"> <small> <?php echo $configuracao['opt_endereco_centro'] ?> </small> </a>
					<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_novo_mundo'] ?>" target="_blank" class="enderecosContatoRodape1 hiddeInfosFooter displayNone"> <small> <?php echo $configuracao['opt_endereco_novo_mundo'] ?> </small> </a>
					<a href="https://www.google.com.br/maps/place/<?php echo $configuracao['opt_endereco_Araucaria'] ?>" target="_blank" class="enderecosContatoRodape2 hiddeInfosFooter displayNone"> <small> <?php echo $configuracao['opt_endereco_Araucaria'] ?> </small> </a>


					<div class="enderecosContatoRodape0 hiddeInfosFooter displayNone">
						<div class="telefones">
							<p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_centro'] ?></p>
							<p><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_centro'] ?></p>
						</div>
						<?php $horario = explode("|",$configuracao['opt_horario_centro']); ?>
						<span><?php echo $horario[0] ?></span>
						<span><?php echo $horario[1] ?></span>
					</div>

					<div class="enderecosContatoRodape1 hiddeInfosFooter displayNone">
						<div class="telefones">
							<p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_novo_mundo'] ?></p>
							<p><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_novo_mundo'] ?></p>
						</div>

						<?php $horario = explode("|",$configuracao['opt_horario_novo_mundo']); ?>
						<span><?php echo $horario[0] ?></span>
						<span><?php echo $horario[1] ?></span>
					</div>

					<div class="enderecosContatoRodape2 hiddeInfosFooter displayNone">
						<div class="telefones">
							<p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $configuracao['opt_telefone_Araucaria'] ?></p>
							<p><i class="fa fa-whatsapp" aria-hidden="true"></i> <?php echo $configuracao['opt_whatsapp_Araucaria'] ?></p>
						</div>

						<?php $horario = explode("|",$configuracao['opt_horario_Araucaria']); ?>
						<span><?php echo $horario[0] ?></span>
						<span><?php echo $horario[1] ?></span>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="mapaSite termos">
					<strong class="visible">Telefone</strong>
					<span>Resposável Técnico <br> Dra. Fabiana Hage de Souza CRO/PR 21515</span>
					<a href="<?php echo home_url('/termos-de-uso/'); ?>">Termos de Uso</a>
					<a href="<?php echo home_url('/politica-de-privacidade'); ?>"> Política de Privacidade</a>

				</div>
			</div>
		</div>
	</div>

	<div class="iconesRodape">
		<div class="container">
			<div class="row ">
				<div class="col-xs-6 logoStardente">
					<img src="<?php echo $configuracao['opt_logoRodape']['url'] ?>" alt="Stardente">
				</div>
				<div class="col-xs-6 redesocial">
					<a href="https://www.instagram.com/stardente/?hl=pt-br" target="_blank">
						<i class="fa fa-instagram"></i>
					</a>
					<a href="<?php echo $configuracao['opt_face'] ?>" target="_blank">
						<i class="fa fa-facebook-official" aria-hidden="true"></i>
					</a>
				</div>
			</div>
		</div>
	</div>

</footer>
<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<p>© <?php echo $configuracao['opt_Copyryght'] ?></p>
			</div>
			<div class="col-sm-6 text-right">
				<span>Desenvolvido por</span>
			
				<a href="http://www.handgran.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/logoCopyrightHandgran.png" alt=""></a>
			</div>
		</div>
	</div>
</div>
	
<?php wp_footer(); ?>

</body>
</html>
