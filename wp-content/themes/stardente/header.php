<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stardente
 */
global $configuracao;
$doutores_centro = explode("|",$configuracao['opt_dr_centro']);
$doutores_novo_mundo = explode("|",$configuracao['opt_dr_novo_mundo']);
$doutores_araucaria = explode("|",$configuracao['opt_dr_Araucaria']);
?>
<!doctype html>
<html <?php language_attributes(); ?>>


<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name=“google-site-verification” content=“4NaTak1F90Izlz8tTRW7F3tOC3PH_pytf87kftcDS6Y” />

	<?php wp_head(); ?>

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5DT8BL');</script>
<!-- End Google Tag Manager -->




</head>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DT8BL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<body <?php body_class(); ?>>
<div class="btnAgendar">
	<a href="https://api.whatsapp.com/send?l=pt&amp;phone=55041999150726" data-title="<?php echo get_the_title() ?> " target="_blank" id="cliquebtnWhatsappFloat"> <span>Agende sua cosulta pelo whats!</span> <i class="fa fa-whatsapp" aria-hidden="true"></i> </a>
</div>

<div class="telefoneLigar">
	<a href="tel:55041999150726"><i class="fa fa-phone" aria-hidden="true"></i> Agende por telefone</a>
</div>



<div class="menuModile">
	<button id="btnMenuClose">X</button>
	<div class="navbar" role="navigation">	
		<!--  MENU MOBILE-->
		<div class="row navbar-header">			
			<nav class="navbar-collapse">
				<?php 
						$menu = array(
							'theme_location'  => '',
							'menu'            => 'Menu Principal',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'nav navbar-nav',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 2,
							'walker'          => ''
							);
						wp_nav_menu( $menu );
					?>
			</nav>						
		</div>			
	</div>
</div>
<!-- TOPO -->
<header class="topo">
	<div class="container">
		<div class="row">

			<!-- LOGO -->
			<div class="col-sm-3 col-xs-9">
				<a href="<?php echo home_url('/'); ?>" class="logo">
					<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo get_bloginfo() ?>" class="img-responsive">
				</a>
			</div>

			<!-- MENU -->
			<div class="col-sm-9 col-xs-3">
				<div class="navbar" role="navigation">	

					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botaomenu" class="navbar-toggle collapsed hvr-pop">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
							<?php 
									$menu = array(
										'theme_location'  => '',
										'menu'            => 'Menu Principal',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $menu );
								?>
						</nav>						
					</div>			
				</div>
			</div>

		</div>
	</div>
</header>


<script>
	  $("#botaomenu").click(function(e){
        $(".menuModile").addClass("openMenu");
        $("body").addClass("scrollStop");

    }); 
	  $("#btnMenuClose").click(function(e){
        $(".menuModile").removeClass("openMenu");
        $("body").removeClass("scrollStop");
    }); 
	 $(".menu-item-291>a").click(function(){ 
		event.preventDefault();
	});
</script>